---
title: Bianco
description: Bianco
#bgcolor: '#ececec'
#textcolor: rgb(210, 210, 209)
bgcolor: rgb(240, 240, 240)
textcolor: rgb(40, 40, 40)
img: images/biancobianco-detail.png
alt: Bianco Bianco Wine bottles
sort: 71
author:
  - name: malte ludwig
    cost: $1
---
