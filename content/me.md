I am:
<br>
<br>
Experienced (> 15 years) front-end engineer and designer based in Paris, France (UTC+2).<br>
I have realised lots of tailor-made websites for luxury brands. Let's say i know that quality of execution matters and how to handle tight deadlines and high expectations.<br>
I enjoy working with nuxt, tailwind and JamStack architecture but i'm open to other stacks.
<br>
<br>
<br>
I do:
<br>
<br>
Frontend Development / Fullstack Development / Conception / Prototypes /
Visual Identies / Printdesign, Webesign
<br>

<br>
<br>
With:
<br>
<br>
Javascript / Vue.js / Nuxt.js / Html5 / Css / Css3 / Sass / Tailwind / Buetify / Bulma  / Git  / Directus / Cockpit Cms / Codeingiter / Cake / Netlify / Adobe / Figma / Sketch
<br>
<br>
<br>
For:
<br>
<br>
Boucheron / Bureau Betak / Bonpoint / Cartier / Chanel / Collette / Fondation Cartier / Hermès / Issey Miyake Parfums / Louis Vuitton / Kering / Kering Eyewear / Le Royal Monceau / Marie Hélène De Taillac / Missoni / Nina Ricci / Nuun Jewels / Paco Rabanne / Emilio Pucci
