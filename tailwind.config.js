/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    // Docs : https://tailwindcss.com/docs/theme/#theme-structure
    screens: {
      sm: '640px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '1024px',
      // => @media (min-width: 1024px) { ... }

      xl: '1280px',
      // => @media (min-width: 1280px) { ... }

      xxl: '1920px',
      // => @media (min-width: 1920px) { ... }
    },
    // Docs: https://tailwindcss.com/docs/font-family/#customizing
    fontFamily: {
      sans: ['BrownStd', 'sans-serif'],
      light: ['BrownStdLight', 'serif'],
      bold: ['BrownStdBold', 'serif'],
      serif: ['AwConquerorDidotLight', 'serif'],
    },
    // Docs: https://tailwindcss.com/docs/customizing-colors/#app
    colors: {
      black: '#000000',
      green: {
        lighter: '#948E7B',
        default: '#69685B',
      },
      blue: '#465060',
      grey: {
        lighter: '#f5f1f1',
        default: '#b3b3b3',
        dark: '#4d4d4d',
      },
      white: '#ffffff',
    },
    extend: {
      opacity: {
        10: '0.1',
        15: '0.15',
        20: '0.2',
        95: '0.95',
      },
    },
  },
  variants: {},
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
  },
}
